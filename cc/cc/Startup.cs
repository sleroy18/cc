﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(cc.Startup))]
namespace cc
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
